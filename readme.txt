This project is aimed to compare peroformace of different languages by finding primary numbers in the given range. 

After running SW, computational time in seconds and number of found elements will be printed out.

At this point only python and C++ are compared.

=======Pyhton=======
To run python script, type in console (see screen shot python-300000-upperBound.png):
"python perf_test.py [upperBound]"

Where: 
[upperBound] - Upper bound of the search - an integer.

=======C++=======
To run binary, type in console (see screen shot C++-300000-upperBound.png):
"\Debug\perf_test.exe [upperBound]"

Where: 
[upperBound] - Upper bound of the search - an integer.