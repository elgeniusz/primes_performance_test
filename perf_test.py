import datetime
import argparse

def primes(upperBound):
	outputArray = []
	initialTime = datetime.datetime.now()
	for n in range(2, upperBound):
		for x in range(2, n):
			if n % x == 0:
				break
		else:
			outputArray.append(n)
	
	print len(outputArray)
	return  datetime.datetime.now() - initialTime

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description = 'This module searches for prime number in the given range, and calculates needed time.')
	parser.add_argument('upperBound', type = int, help = 'Upper bound of the search.')
								
	args = parser.parse_args()
	result = primes(args.upperBound)
	print result.seconds

