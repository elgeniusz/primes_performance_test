#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <ctime>

static void primes(int upperBound);

int main(int argc, char **argv)
{
    if(argc == 2)
    {
        char* p;
        int upperBound = strtol(argv[1], &p, 10);
        primes(upperBound);
    }
    else
    {
        std::cout << "Wrong number of arguments. Please give just one integer which is and upper bound of search prime function.";
    }
    
	return 0;
}

static void primes(int upperBound)
{
    std::vector<int> primes;
    time_t initialTime = time(NULL);

    for(int n = 2; n < upperBound; n++)
    {
        int x = 2;
        for(; x < n; x++)
        {
            if(n % x == 0)
            {
                break;
            }
        }
        
        if(x == n)
        {
            primes.push_back(n);
        }
    }

    std::cout << primes.size() << "\r\n";
    double diff = difftime(time(NULL) ,initialTime);
    std::cout << diff;
}